abstract class Notification {
  Future<void> notify();

  Future<void> dismiss();
}
